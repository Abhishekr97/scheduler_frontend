import React from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import { CardElements, BulletPoints } from "./LandingPageElements";
import Header from "../header/Header";
import Footer from "../footer/Footer";
import { auto } from "@popperjs/core";

const LandingPage: React.FC = () => {
  return (
    <div>
      <Header />

      <div style={{ margin: "20px" }}>
        <h2 style={{ color: "GrayText", margin: "20px" }}>
          Simplify Scheduling, Supercharge Productivity with AWS
        </h2>
        <h3 style={{ textAlign: "center", color: "#ab7cda" }}>
          Try AWS for free.
        </h3>
        <h3>
          Make scheduling with clients and recruits easier with a free AWS
          account. First-time signups also receive a free, 14-day trial of our
          Teams subscription plan!
        </h3>
      </div>

      {/* <Container style={{ boxShadow: "none" }}> */}
      <div style={{margin: '30px'}}>
        <Row>
          {CardElements.map((item, index) => {
            return (
              <Col key={index} sm={6} lg={3} className="card-height mb-4">
                <Card className="h-80 d-flex flex-column">
                  <Card.Img variant="top" src={item.img} alt="Calendly Image" />
                  <Card.Body className="d-flex flex-column">
                    <Card.Title>{item.quote1}</Card.Title>
                    <Card.Text>{item.quote2}</Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            );
          })}
        </Row>
      </div>
      {/* </Container> */}

      <div className="data-listen">
        <ol className="list-unstyled">
          {BulletPoints.map((item, index) => {
            return (
              <div className="bullet" style={{display:"flex",justifyContent:"space-between",background:"#bdb9af" ,border:"solid #14416e 5px" , borderRadius: "10px"}}>
                <li
                  key={index}
                  className="bullet-list d-flex justify-content-around p-2"
                >
                <div style={{width:"20px"}}>
                <i className="fa fa-check-circle-o text-success fa-x" aria-hidden="true"></i>


                </div>
                <div>
                <h5>{item.quote}</h5>
                </div>
                </li>
              </div>
            );
          })}
        </ol>
      </div>
      <Footer />
    </div>
  );
};

export default LandingPage;
