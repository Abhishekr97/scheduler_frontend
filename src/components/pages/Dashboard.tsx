import React from "react";
import Banner from "../common/Banner";
import Header from "../header/Header";

function Dashboard() {
  return (
    <div>
      <Header />
      <Banner />
    </div>
  );
}

export default Dashboard;
