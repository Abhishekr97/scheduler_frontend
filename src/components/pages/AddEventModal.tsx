import React, { useState } from "react";
import Modal from "react-modal";
import DateTime from "react-datetime";
import moment, { Moment } from "moment";

interface EventData {
  title: string;
  start: Date;
  end: Date;
}

interface AddEventProps {
  isOpen: boolean;
  onClose: () => void;
  onEventAdded: (event: EventData) => void;
}

const AddEventModal: React.FC<AddEventProps> = ({
  isOpen,
  onClose,
  onEventAdded,
}) => {
  const [title, setTitle] = useState<string>("");
  const [start, setStart] = useState<Date>(new Date());
  const [end, setEnd] = useState<Date>(new Date());

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    onEventAdded({
      title,
      start,
      end,
    });

    onClose();
  };

  const handleStartChange = (date: Moment | string) => {
    if (moment.isMoment(date)) {
      setStart(date.toDate());
    } else {
      setStart(new Date(date));
    }
  };

  const handleEndChange = (date: Moment | string) => {
    if (moment.isMoment(date)) {
      setEnd(date.toDate());
    } else {
      setEnd(new Date(date));
    }
  };

  return (
    <Modal isOpen={isOpen} onRequestClose={onClose} >
      <form onSubmit={onSubmit}>
        <input
          placeholder="Title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />

        <div>
          <label htmlFor="start">Start Date</label>
          <DateTime value={moment(start)} onChange={handleStartChange} />
        </div>

        <div>
          <label htmlFor="end">End Date</label>
          <DateTime value={moment(end)} onChange={handleEndChange} />
        </div>

        <div className="text-center mt-4 d-flex justify-content-end">
          <button className="btn btn-primary mx-3">Add Event</button>
        </div>
      </form>
    </Modal>
  );
};

export default AddEventModal;


// <div
    //   className={`modal ${isOpen ? "show" : ""}`}
    //   style={{ display: isOpen ? "block" : "none" }}
    // >
    //   <div className="modal-overlay" onClick={onClose}></div>
    //   <div className="modal-dialog">
    //     <div className="modal-content">
    //       <div className="modal-header">
    //         <h5 className="modal-title">Schedule Meeting</h5>
    //         <button type="button" className="close" onClick={onClose}>
    //           <span>&times;</span>
    //         </button>
    //       </div>
    //       <div className="modal-body">
    //         <form onSubmit={onSubmit}>
    //           <input
    //             placeholder="Title"
    //             value={title}
    //             onChange={(e) => setTitle(e.target.value)}
    //           />

    //           <div>
    //             <label htmlFor="start">Start Date</label>
    //             <DateTime value={moment(start)} onChange={handleStartChange} />
    //           </div>

    //           <div>
    //             <label htmlFor="end">End Date</label>
    //             <DateTime value={moment(end)} onChange={handleEndChange} />
    //           </div>

    //           <div className="text-center mt-4 d-flex justify-content-end">
    //             <button className="btn btn-primary mx-3">Add Event</button>
    //           </div>
    //         </form>
    //       </div>
    //     </div>
    //   </div>
    // </div>
