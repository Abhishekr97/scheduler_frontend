import cardPic from "../assets/calendly-background.webp";
import cardPic1 from "../assets/home-page-pic-1.jpeg";
import cardPic2 from "../assets/cardPic2.jpg";
import cardPic3 from "../assets/cardPic3.webp";

export const NavbarElements = [
  {
    title: "Home",
    link: "",
  },
  {
    title: "About",
    link: "/about",
  },
  {
    title: "Contacts",
    link: "/contacts",
  },
];

export const DashboardElements = [
  {
    title: "Home",
    link: "/home",
  },
  {
    title: "About",
    link: "/about",
  },
  {
    title: "Contacts",
    link: "/contacts",
  },
];

export const CardElements = [
  {
    img: cardPic,
    quote1: "Welcome to And We Suppot",
    quote2: "Schedule meetings with ease.",
  },
  {
    img: cardPic2,
    quote1: "Make Effective your meetings with our suggestions",
    quote2: "Schedule meetings with ease.",
  },
  {
    img: cardPic1,
    quote1: "Save your time ",
    quote2: "Schedule meeting in 1 click.",
  },
  {
    img: cardPic3,
    quote1: "Connect with professionals with our app",
    quote2: "Schedule meeting in 1 click.",
  },
];

export const BulletPoints = [
  {
    quote: "Unlock Your Productivity Potential with AWS.",
  },
  {
    quote: "Elevate Your Business with Smart Scheduling",
  },
  {
    quote: "Efficiency Starts Here: Schedule with Confidence",
  },
  {
    quote: "Booking Bliss: Elevate Your Scheduling Experience",
  },
];
