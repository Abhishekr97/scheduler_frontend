import React, { useRef, useState } from "react";
import AddEventModal from "./AddEventModal";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";

interface EventData {
  title: string;
  start: Date;
  end: Date;
}

const Schedule = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const calendarRef = useRef<FullCalendar | null>(null);

  const onEventAdded = (e: EventData) => {
    if (calendarRef.current) {
      let calendarApi = calendarRef.current.getApi();
      calendarApi.addEvent(e);
    }
  };

  return (
    <section className="mt-5">
      <div
        style={{ position: "relative", zIndex: 0 }}
        className="row d-flex justify-content-center"
      >
        <div className="col-sm-6">
          <FullCalendar
            ref={calendarRef}
            plugins={[dayGridPlugin]}
            initialView="dayGridMonth"
          />
        </div>
      </div>

      <AddEventModal
        isOpen={modalOpen}
        onClose={() => setModalOpen(false)}
        onEventAdded={(e) => onEventAdded(e)}
      />

      <div className="text-center mt-4">
        <button className="btn btn-primary mx-3" onClick={() => setModalOpen(true)}>Add Event</button>
      </div>
      
    </section>
  );
};

export default Schedule;
