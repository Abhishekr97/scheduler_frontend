import React, { useEffect } from "react";
import Login from "../auth/Login";
import Register from "../auth/Register";

function LoginRegister() {
  useEffect(() => {
    const signUpButton = document.getElementById("signUp");
    const signInButton = document.getElementById("signIn");
    const container = document.getElementById("container");

    if (signUpButton && signInButton && container) {
      signUpButton.addEventListener("click", () => {
        container.classList.add("right-panel-active");
      });

      signInButton.addEventListener("click", () => {
        container.classList.remove("right-panel-active");
      });
    }
  }, []);

  return (
    <div className="loginform">
        {/* <div className="data-head">
          <h1>Login/Register Form</h1>
        </div> */}
      <div className="container" id="container">
        <div className="form-container sign-up-container">
          <div className="data">
            <h1>Create Account</h1>
            <div className="social-container">
              <a href="#" className="social">
                <i className="fa fa-facebook" aria-hidden="true"></i>
              </a>
              <a href="#" className="social">
                <i className="fa fa-google" aria-hidden="true"></i>
              </a>
              <a href="#" className="social">
                <i className="fa fa-linkedin" aria-hidden="true"></i>
              </a>
            </div>
            <span>or use your email for registration</span>
            <Register />
          </div>
        </div>
        <div className="form-container sign-in-container">
          <div className="data">
            <h1>Sign in</h1>
            <div className="social-container">
              <a href="#" className="social">
                <i className="fa fa-facebook" aria-hidden="true"></i>
              </a>
              <a href="#" className="social">
                <i className="fa fa-google" aria-hidden="true"></i>
              </a>
              <a href="#" className="social">
                <i className="fa fa-linkedin" aria-hidden="true"></i>
              </a>
            </div>
            <span>or use your account</span>
            <Login />
          </div>
        </div>
        <div className="overlay-container">
          <div className="overlay">
            <div className="overlay-panel overlay-left">
              <h1>Welcome Back!</h1>
              <p>
                To keep connected with us please login with your personal info
              </p>
              <button className="btn btn-outline-danger text-white" id="signIn">
                Sign In
              </button>
            </div>
            <div className="overlay-panel overlay-right">
              <h1>Hello, Friend!</h1>
              <p>Enter your personal details and enjoy with us</p>
              <button className="btn btn-outline-danger text-white" id="signUp">
                Sign Up
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginRegister;
