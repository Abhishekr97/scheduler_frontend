import React, { ChangeEvent, useEffect, useState } from "react";
import { registerUser } from "../services/Api"; // Import the API function
// import { login } from "../services/Api";
import { toast } from "react-toastify";

function Register() {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
  });

  const [errors, setErrors] = useState({
    name: "",
    email: "",
    password: "",
  });

  const [allerror, setAllerror] = useState(false);
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    // Use a setTimeout to clear the success message after 2 seconds
    if (success) {
      const successTimeout = setTimeout(() => {
        setSuccess(false);
      }, 2000); // 2000 milliseconds = 2 seconds

      // Clean up the timeout when the component unmounts
      return () => clearTimeout(successTimeout);
    }
  }, [success]);

  const validateName = () => {
    const nameRegex = /^[a-zA-Z]{2,15}$/;
    if (!nameRegex.test(formData.name)) {
      setErrors({
        ...errors,
        name: "Enter valid name",
      });
    } else {
      setErrors({ ...errors, name: "" });
    }
  };

  const validateEmail = () => {
    // Regular expression for validating an Email
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(formData.email)) {
      setErrors({ ...errors, email: "Enter a valid email address" });
    } else {
      setErrors({ ...errors, email: "" });
    }
  };

  const validatePassword = () => {
    // Regular expressions for password validation
    const uppercaseRegex = /[A-Z]/;
    const specialCharRegex = /[!@#$%^&*(),.?":{}|<>]/;
    const numberRegex = /[0-9]/;

    if (
      formData.password.length < 8 ||
      formData.password.length > 10 ||
      !uppercaseRegex.test(formData.password) ||
      !specialCharRegex.test(formData.password) ||
      !numberRegex.test(formData.password)
    ) {
      setErrors({
        ...errors,
        password: "Password must be 8-10 characters long.",
      });
    } else {
      setErrors({ ...errors, password: "" });
    }
  };

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });

    // Validate fields when the user types
    if (name === "name") {
      validateName();
    } else if (name === "email") {
      validateEmail();
    } else if (name === "password") {
      validatePassword();
    }

     // Check if any of the fields are missing and setAllerror accordingly
  if (!formData.name || !formData.email || !formData.password) {
    setAllerror(true);
  } else {
    setAllerror(false);
  }
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
  
    // Set these states to false initially
    setAllerror(false);
    setSuccess(false);
  
    if (!formData.name || !formData.email || !formData.password) {
      // One or more form fields are missing, setAllerror to true
      setAllerror(true);
    } else {
      // All form fields are provided, proceed with registration
      try {
        setAllerror(false);
        await registerUser(formData); // Send data to the backend
        toast.success("Register successful");
  
        setFormData({ email: "", password: "", name: "" }); // Clear the form data
      
        setSuccess(true);
      } catch (error) {
        console.error("Registration failed:", error);
        toast.error("Register failed");
      }
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className="formdata">
          <div className="py-1">
            <input
              type="text"
              name="name"
              placeholder="Name"
              className="py-2 rounded border"
              value={formData.name}
              onChange={handleInputChange}
            />
            <div>
              <span className="error-text text-danger">{errors.name}</span>
            </div>
          </div>
          <div className="py-1">
            <input
              type="email"
              name="email"
              placeholder="Email"
              className="py-2 rounded border"
              value={formData.email}
              onChange={handleInputChange}
            />
            <div>
              <span className="error-text text-danger">{errors.email}</span>
            </div>
          </div>
          <div className="py-1">
            <input
              type="password"
              name="password"
              placeholder="Password"
              className="py-2 rounded border"
              value={formData.password}
              onChange={handleInputChange}
            />
            <div>
              <span className="error-text text-danger">{errors.password}</span>
            </div>
          </div>
          <div>
            {allerror ? (
              <span className="text-danger">All fields are mandatory</span>
            ) : (
              ""
            )}
            {success ? (
              <span className="text-success">Register successfully</span>
            ) : (
              ""
            )}
          </div>
          <button className="btn btn-danger py-2 my-2" type="submit">
            Register
          </button>
        </div>
      </form>
    </div>
  );
}

export default Register;