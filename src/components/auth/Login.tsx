 import React from "react";
 import { useForm } from "react-hook-form";
import { login } from "../services/Api";
import { toast } from "react-toastify";
import { useNavigate } from "react-router";


function Login() {
// <<<<<<< HEAD
  const navigate = useNavigate();
// =======



// >>>>>>> implement_RememberMe
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset, // Add the reset function
  } = useForm<FormValues>();
  type FormValues = {
    email: string;
    password: string;
    rememberMe: boolean;

  };

  const onSubmit = async (data: FormValues) => {
    try {
      await login(data);
      toast.success("Login successful");
      navigate('/dashboard')

      reset();
    } catch (error:any) {
      if(error.response.status===400 || error.response.status===404){
        toast.error(error.respnse.data.message);

      }else{
        toast.error("something went wrong on server !!")
      }
    }
  };



  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="formdata">
          <div className="py-1">
            <input
              type="email"
              {...register("email", { required: true })}
              placeholder="Email"
              className={
                "py-2 rounded border " + (errors.email ? "is-invalid" : "")
              }
            />

            {errors.email && (
              <span className="error invalid-feedback">
                {" "}
                This field is required !{" "}
              </span>
            )}
          </div>

          <div className="py-1">
            <input
              type="password"
              {...register("password", { required: true })}
              placeholder="Password"
              className={
                "py-2 rounded border " + (errors.password ? "is-invalid" : "")
              }
            />
            {errors.password && (
              <span className="error invalid-feedback">
                {" "}
                This field is required !{" "}
              </span>
            )}

            <div className="my-1">
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="checkbox"
                  {...register("rememberMe")}
                  id="rememberMe"
                />
                <span>
                  Remember Me
                </span>
                <span>
                  Forgot Password?
                </span>

              </div>

              {/* <a href="#">Forgot your password?</a> */}
            </div>
            <button type="submit" className="my-1 btn btn-primary">
              Sign In
            </button>
           

          </div>
        </div>
      </form>
    </div>
  );
}

export default Login;
