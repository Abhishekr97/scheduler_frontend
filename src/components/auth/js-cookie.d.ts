declare module 'js-cookie' {
    const Cookies: {
      get(name: string): string | undefined;
      set(name: string, value: string, options?: Object): void;
      remove(name: string, options?: Object): void;
      // Add other methods you intend to use from js-cookie
    };
  
    export = Cookies;
  }