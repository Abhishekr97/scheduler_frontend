import { useState, useEffect } from "react";

const Footer = () => {
  const [isFooterFixed, setIsFooterFixed] = useState(true);

  useEffect(() => {
    const handleScroll = () => {
      const contentHeight = document.body.scrollHeight;
      const viewportHeight = window.innerHeight;

      // Check if content is less than viewport height
      if (contentHeight < viewportHeight) {
        setIsFooterFixed(true);
      } else {
        setIsFooterFixed(false);
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const footerClassName = `footer bg-dark text-white text-center py-3 ${
    isFooterFixed ? "fixed-bottom" : ""
  }`;

  return (
    <footer className={footerClassName}>
      &copy; {new Date().getFullYear()} And We Support. All rights reserved.
    </footer>
  );
};

export default Footer;
