import React, { useState } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { NavDropdown } from 'react-bootstrap';
import {
  NavbarElements,
  DashboardElements,
} from "../pages/LandingPageElements";
import admin from "../assets/admin.jpg";
import Cookies from 'js-cookie';
import CustomModal from '../common/Modal'; // Import the CustomModal component

const Header: React.FC = () => {
  const navigate = useNavigate();
  const token = Cookies.get('jwtoken');
  const [showModal, setShowModal] = useState(false); // State to manage modal visibility

  const logout = () => {
    Cookies.remove("jwtoken");
    navigate("/");
  };

  // Function to open the modal
  const openModal = () => {
    setShowModal(true);
  };

  // Function to close the modal
  const closeModal = () => {
    setShowModal(false);
  };

  return (
    <div>
      <Navbar bg="dark" expand="sm" data-bs-theme='dark' className="py-1 fixed-top">
        <Navbar.Brand className="mx-4" href="/">
          SchedulerAWS
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse className="ml-3 px-2" id="basic-navbar-nav">
          <Nav className="ml-auto px-3" >
            <ul className="nav">
              {token
                ? DashboardElements.map((item, index: number) => {
                    return (
                      <li key={index} className="nav-item">
                        <Link
                          to={item.link}
                          className="nav-link text-white my-1 fs-6 text-lg"
                        >
                          {item.title}
                        </Link>
                      </li>
                    );
                  })
                : NavbarElements.map((item, index: number) => {
                    return (
                      <li key={index} className="nav-item">
                        <Link
                          to={item.link}
                          className="nav-link my-1 text-white fs-6 text-lg"
                        >
                          {item.title}
                        </Link>
                      </li>
                    );
                  })}
            </ul>
          </Nav>
          {token ? (
            <NavDropdown
              title={
                <img src={admin} alt="User Avatar" className="img-fluid" style={{width: "35px", height: "35px", borderRadius: "50%"}}/>
              }
              id="basic-nav-dropdown"
              align="end"
            >
              <NavDropdown.Item href="#">{`Profile :${token}`}</NavDropdown.Item>     
              <NavDropdown.Divider />
              <NavDropdown.Item onClick={logout}>Logout</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item onClick={openModal}>Your Availability</NavDropdown.Item> {/* Open the modal when clicked */}
            </NavDropdown>
          ) : (
            <Link to="/login" className="btn btn-primary mx-3">
              Get Started
            </Link>
          )}
        </Navbar.Collapse>
      </Navbar>

      <CustomModal show={showModal} onHide={closeModal} /> {/* Pass the show and onHide props to the modal */}
    </div>
  );
};

export default Header;
