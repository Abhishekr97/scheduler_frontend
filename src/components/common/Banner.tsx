import React from "react";
import Carousel from 'react-bootstrap/Carousel';
import CarouselImage from './CarouselImage'; // Import the CarouselImage component

import card1 from "../assets/calendly-background.webp";
import card2 from "../assets/cardPic2.jpg";
import card3 from "../assets/cardPic3.webp";

function Banner() {
  return (
    <div>
      <Carousel>
        <Carousel.Item interval={1000}>
          <CarouselImage imageSrc={card1} text="First slide" />
          <Carousel.Caption>
            <h3>First Image</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={500}>
          <CarouselImage imageSrc={card2} text="Second slide" />
          <Carousel.Caption>
            <h3>Second Image</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <CarouselImage imageSrc={card3} text="Third slide" />
          <Carousel.Caption>
            <h3>Third Image</h3>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}

export default Banner;
