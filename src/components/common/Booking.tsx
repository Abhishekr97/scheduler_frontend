import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createBooking } from "../services/Api"; // Import the new API function

function Booking() {
  const [startDate, setDate] = useState<Date>(new Date());
  const defaultEndDate = new Date();
  defaultEndDate.setDate(defaultEndDate.getDate() + 7);
  const [rangeStart, setRangeStart] = useState<Date>(new Date());
  const [rangeEnd, setRangeEnd] = useState<Date>(defaultEndDate);
  const today = new Date();

  const selectStartDate = (d: Date) => {
    if (d && d <= rangeEnd) { // Check if d is not null and less than or equal to rangeEnd
      setRangeStart(d);
    }
  };

  const selectEndDate = (d: Date) => {
    if (d && d >= rangeStart) { // Check if d is not null and greater than or equal to rangeStart
      setRangeEnd(d);
    }
  };

  // Initially, set the button as disabled
  const [isSubmitButtonDisabled, setIsSubmitButtonDisabled] = useState(true);

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    // Prepare the data in the required format
    const bookingData = [{ startDate: rangeStart, endDate: rangeEnd }];

    try {
      const response = await createBooking(bookingData);
      // Handle the response as needed (e.g., show a success message)
      console.log(response);
    } catch (error) {
      // Handle errors (e.g., display an error message)
      console.error(error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="row">
        <div className="col-sm-6">
          <div className="startdate">
            <label>Select Start Date</label>
            <DatePicker
              selected={rangeStart}
              minDate={today}
              maxDate={rangeEnd}
              onChange={(date) => {
                selectStartDate(date as Date); // Cast date to Date type
                setIsSubmitButtonDisabled(date === null || date >= rangeEnd); // Update button disabled state
              }}
            />
          </div>
        </div>
        <div className="col-sm-6">
          <div className="startdate">
            <label>Select End Date</label>
            <DatePicker
              selected={rangeEnd}
              minDate={rangeStart}
              onChange={(date) => {
                selectEndDate(date as Date); // Cast date to Date type
                setIsSubmitButtonDisabled(date === null || date <= rangeStart); // Update button disabled state
              }}
            />
          </div>
        </div>
        <div className="my-3 d-flex justify-content-end">
          <button
            className="btn btn-primary"
            type="submit"
            disabled={isSubmitButtonDisabled} // Disable the button if the start date is not less than the end date
          >
            Submit
          </button>
        </div>
      </div>
    </form>
  );
}

export default Booking;
