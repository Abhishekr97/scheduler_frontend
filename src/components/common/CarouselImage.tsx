import React from "react";

interface CarouselImageProps {
  imageSrc: string;
  text: string;
}

const imageStyle = {
  width: "100%", // Adjust the width as needed
  height: "90vh", // Adjust the height as needed
};

function CarouselImage({ imageSrc, text }: CarouselImageProps) {
  return (
    <img src={imageSrc} alt={text} style={imageStyle} />
  );
}

export default CarouselImage;
