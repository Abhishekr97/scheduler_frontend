import React from "react";
import cardpic from "../assets/cardPic2.jpg";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";

function Navbar() {
  const navigate = useNavigate();

  const logout = () => {
    localStorage.removeItem("userDetail");
    navigate("/login");
  };

   // Function to show the dropdown on hover
   const handleImageHover = () => {
    const dropdownMenu = document.querySelector(".dropdown-menu") as HTMLDivElement;
    if (dropdownMenu) {
      dropdownMenu.style.display = "block";
    }
  };

  // Function to hide the dropdown when the mouse leaves
  const handleImageLeave = () => {
    const dropdownMenu = document.querySelector(".dropdown-menu") as HTMLDivElement;
    if (dropdownMenu) {
      dropdownMenu.style.display = "none";
    }
  };

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link to='/' className="navbar-brand">
            AndWeSupport
          </Link>
          <Link to="/schedule">Schedule</Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0" style={{ visibility: "hidden" }}>
              <li className="nav-item">
                <Link to='/profile' className="nav-link active" aria-current="page">
                  Profile
                </Link>
              </li>
              <li className="nav-item">
                <Link to='/contact' className="nav-link">
                  Contact
                </Link>
              </li>
            </ul>
            <div className="d-flex align-items-center justify-content-center gap-2 userDetails">
              <div className="username display-6">Username</div>
              <div
                className="userimage  position-relative"
                onMouseEnter={handleImageHover}
                onMouseLeave={handleImageLeave}
              >
                <img alt="card" src={cardpic} className="img-fluid" />
                <div className="dropdown-menu position-absolute" style={{ display: "none"}}>
                  <Link to='/profile' className="dropdown-item">
                    Profile
                  </Link>
                  <Link to='logout' className="dropdown-item" onClick={logout}>
                    Logout
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
