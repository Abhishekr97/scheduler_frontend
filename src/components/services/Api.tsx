import axios from 'axios';
import Cookies from 'js-cookie'; // Import the js-cookie library

const BASE_URL = 'http://localhost:3008/api/v1'; // Replace this with your backend API base URL

const api = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export const registerUser = async (userData: any) => {
  try {
    const response = await api.post('/userRegister', userData);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const login = async (LoginformData: any) => {

  try {
    const response = await api.post('/login', LoginformData);
    console.log("response",response)
    if (LoginformData.rememberMe || response.data.code===200) {
      // Set the token in a cookie using js-cookie
      Cookies.set('jwtoken',(response.data.userData.name), { expires: 7 }); // Token will expire in 7 days // Token will expire in 7 days
    }
    // if(response.data.code===200){
    //   window.sessionStorage.setItem('jwtoken',response.data.token);
    // }
 console.log("respnse.data",response.data)
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const createBooking = async (bookingData: any) => {
  try {
    const response = await api.post('/booking', bookingData);
    return response.data;
  } catch (error) {
    throw error;
  }
};