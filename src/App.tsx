import React from "react";
import "./App.css";
import Routing from "./Routing";
import { BrowserRouter } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Modal from 'react-modal';
Modal.setAppElement("#root");

function App() {
  return (
    <div className="App">
      <ToastContainer />
      <BrowserRouter>
        <Routing />
      </BrowserRouter>
    </div>
  );
}

export default App;
