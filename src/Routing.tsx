import React,{useEffect} from "react";
import { Route, Routes } from "react-router";
import LoginRegister from "./components/pages/LoginRegister";
import LandingPage from "./components/pages/landinpages";
import Dashboard from "./components/pages/Dashboard";
import ProtectedRoutes from "./utils/authToken";
import Schedule from "./components/pages/Schedule";
// import { useNavigate } from "react-router";
// import Cookies from 'js-cookie'; // Import js-cookie

// const token = Cookies.get('jwtoken') 

function Routing() {

  // const navigate = useNavigate();
  // useEffect(() => {
  //   if (token ) {
  //     // Redirect to the login page if not authenticated
  //     navigate('/dashboard');

  //   } else {
  //     // Redirect to the dashboard if authenticated
  //     navigate('/');

  //   }
  // }, [navigate]);



  return (
    <Routes>
      <Route path="/" element={<LandingPage />} />
      <Route path="/login" element={<LoginRegister />} />
      <Route
        path="/dashboard"
        element={
          <ProtectedRoutes>
            <Dashboard />
            </ProtectedRoutes>
        }
      />
      <Route
        path="/schedule"
        element={
          <ProtectedRoutes>
            <Schedule />
          </ProtectedRoutes>
        }
      />
    </Routes>
  );
}

export default Routing;
