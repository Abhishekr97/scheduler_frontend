import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie'; // Import js-cookie

const ProtectedRoutes = ({ children }) => {
  const token = Cookies.get('jwtoken') 
  const navigate = useNavigate();
  useEffect(() => {
    if (!token ) {
      // Redirect to the login page if not authenticated
      navigate('/login');

    } 
    // else {
    //   // Redirect to the dashboard if authenticated
    //   navigate('/schedule');

    // }
  }, [token, navigate]);

  return token  ? children : null;
};

export default ProtectedRoutes;
